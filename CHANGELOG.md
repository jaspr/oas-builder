# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.2](https://gitlab.com/jaspr/oas-builder/compare/1.1.1...1.1.2) (2024-12-07)


### Changed

* replace generic Exception to RuntimeException ([be6fc67](https://gitlab.com/jaspr/oas-builder/commit/be6fc671d15f8572205f56c7f2b3941d0dbd4e4f))

### [1.1.1](https://gitlab.com/jaspr/oas-builder/compare/1.1.0...1.1.1) (2023-01-16)


### Fixed

* return type object ([c16d2af](https://gitlab.com/jaspr/oas-builder/commit/c16d2afad81aeef14567c0413a7cfa1533152745))

## [1.1.0](https://gitlab.com/jaspr/oas-builder/compare/1.0.0...1.1.0) (2022-07-14)


### Added

* new data types ([e882f68](https://gitlab.com/jaspr/oas-builder/commit/e882f68f4f98eeafdf82fec4e49accae28200863))


### Changed

* remove tristate ([fa6d283](https://gitlab.com/jaspr/oas-builder/commit/fa6d2833c2e503263b71de70852a9ef7baf1d643))

## 1.0.0 (2022-06-20)


### Changed

* moving project ([fe60c5a](https://gitlab.com/jaspr/oas-builder/commit/fe60c5a6cc93e22b38d499463d4a1f010f78866c))
* remove builder ([f6be8c6](https://gitlab.com/jaspr/oas-builder/commit/f6be8c6eee81d7fa4b0a7c7b88f6e410a7e4ed9f))
* rename project namespace ([08cdcf0](https://gitlab.com/jaspr/oas-builder/commit/08cdcf0608c81a54e7c08f51a2951a6820145d8a))
