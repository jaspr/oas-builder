<?php

/**
 * Created by uzivatel
 * at 13.07.2022 13:51
 */

declare(strict_types=1);

namespace JSONAPI\OAS\Test;

use JSONAPI\OAS\Schema;
use PHPUnit\Framework\TestCase;

class SchemaTest extends TestCase
{
    public function testNew()
    {
        $schema = Schema::new();
        $schema->setType('string');
        $schema->setFormat('date-time');
        $this->assertInstanceOf(Schema::class, $schema);
        $obj = $schema->jsonSerialize();
        $this->assertEquals($obj->format, 'date-time');
    }
}
