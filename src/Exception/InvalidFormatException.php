<?php

declare(strict_types=1);

namespace JSONAPI\OAS\Exception;

/**
 * Class InvalidFormatException
 *
 * @package JSONAPI\Exception\OAS
 */
class InvalidFormatException extends OpenAPIException
{
}
