<?php

declare(strict_types=1);

namespace JSONAPI\OAS\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package JSONAPI\Exception\OAS
 */
class InvalidArgumentException extends OpenAPIException
{
}
