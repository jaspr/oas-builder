<?php

declare(strict_types=1);

namespace JSONAPI\OAS\Exception;

/**
 * Class DuplicationEntryException
 *
 * @package JSONAPI\Exception\OAS
 */
class DuplicationEntryException extends OpenAPIException
{
}
