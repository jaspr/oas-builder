<?php

declare(strict_types=1);

namespace JSONAPI\OAS\Exception;

/**
 * Class IncompleteObjectException
 *
 * @package JSONAPI\Exception\OAS
 */
class IncompleteObjectException extends OpenAPIException
{
}
