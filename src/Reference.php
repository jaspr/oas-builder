<?php

declare(strict_types=1);

namespace JSONAPI\OAS;

use JSONAPI\OAS\Exception\OpenAPIException;

/**
 * Class Reference
 *
 * @package JSONAPI\OAS
 * @template T of Reference
 */
abstract class Reference
{
    /**
     * @var T
     */
    protected mixed $origin;
    /**
     * @var string
     */
    protected string $ref;
    /**
     * @var bool
     */
    protected bool $isRef = false;

    /**
     * @param string $to
     * @param T $origin
     *
     * @return T
     * @throws OpenAPIException
     */
    abstract public static function createReference(
        string $to,
        mixed $origin
    ): mixed;

    /**
     * @return bool
     */
    public function isReference(): bool
    {
        return $this->isRef;
    }

    /**
     * @return object
     */
    public function jsonSerialize(): object
    {
        return (object)['$ref' => $this->ref];
    }

    /**
     * @param string $to
     * @param T $origin
     */
    protected function setRef(string $to, mixed $origin): void
    {
        $this->isRef = true;
        $this->ref = $to;
        $this->origin = $origin;
    }
}
