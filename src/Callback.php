<?php

declare(strict_types=1);

namespace JSONAPI\OAS;

use JSONAPI\OAS\Exception\OpenAPIException;
use ReflectionClass;

/**
 * Class Callback
 *
 * @package JSONAPI\OAS
 * @extends Reference<Callback>
 */
class Callback extends Reference implements \JsonSerializable
{
    /**
     * @var string
     */
    private string $expression;
    /**
     * @var PathItem
     */
    private PathItem $pathItem;

    /**
     * Callback constructor.
     *
     * @param string $expression
     * @param PathItem $pathItem
     */
    public function __construct(string $expression, PathItem $pathItem)
    {
        $this->expression = $expression;
        $this->pathItem   = $pathItem;
    }

    /**
     * @inheritDoc
     */
    public static function createReference(string $to, mixed $origin): Callback
    {
        try {
            /** @var Callback $static */
            $static = (new ReflectionClass(__CLASS__))->newInstanceWithoutConstructor(); // NOSONAR
            $static->setRef($to, $origin);
            return $static;
        } catch (\ReflectionException $exception) {
            throw OpenAPIException::createFromPrevious($exception);
        }
    }

    public function jsonSerialize(): object
    {
        if ($this->isReference()) {
            return parent::jsonSerialize();
        }
        $ret = [
            $this->expression => $this->pathItem
        ];
        return (object)$ret;
    }
}
